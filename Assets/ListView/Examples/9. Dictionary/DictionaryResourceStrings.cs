﻿namespace Unity.Labs.ListView
{
    public static class DictionaryResourceStrings
    {
        public const string editorDatabasePath = "ListView/Examples/9. Dictionary/wordnet30.db";
        public const string databasePath = "wordnet30.db";
    }
}
